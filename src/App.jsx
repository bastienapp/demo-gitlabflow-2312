import "./App.css";
import Contact from "./components/Contact";
import HomePage from "./pages/Home/HomePage";
import GameList from "./pages/GameList/GameList";

import { createBrowserRouter, RouterProvider } from "react-router-dom";

const router = createBrowserRouter([
  {
    path: "/",
    element: <HomePage />,
  },
  {
    path: "/contact",
    element: <Contact />,
  },
  {
    path: "/games",
    element: <GameList />,
  },
]);

function App() {
  return (
    <>
      <RouterProvider router={router} />
    </>
  );
}

export default App;
