import Navigation from "../../components/Navigation";
import Title from "../../components/Title";

function HomePage() {
  return (
    <>
      <Navigation />
      <Title content='Home' />
      <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam et odio
        placeat rem ratione vero nesciunt quibusdam hic aliquid architecto rerum
        itaque atque illo mollitia aliquam, quisquam alias modi ipsa?
      </p>
    </>
  );
}

export default HomePage;
